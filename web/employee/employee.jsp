<%--
  Created by IntelliJ IDEA.
  User: Tinh
  Date: 8/1/2019
  Time: 10:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Form Employee</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
<div class="d-flex justify-content-center align-items-center" style="height: 100%">
    <form method="post" class="col-lg-4 col-sm-6" action="/employee">
        <h4 class="text-center mb-4">Form Employee</h4>
        <div class="form-group">
            <label>Full Name</label>
            <input type="text" class="form-control" name="fullName">
        </div>
        <div class="form-group">
            <label>Birthday</label>
            <input type="text" class="form-control" name="birthday">
        </div>
        <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" name="address">
        </div>
        <div class="form-group">
            <label>Position</label>
            <input type="text" class="form-control" name="position">
        </div>
        <div class="form-group">
            <label>Department</label>
            <input type="text" class="form-control" name="department">
        </div>
        <input type="submit" class="btn btn-primary btn-block" value="Submit">
        <input type="reset" class="btn btn-primary btn-block" value="Reset">
    </form>
</div>
</body>
</html>
