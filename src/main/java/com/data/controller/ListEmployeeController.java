package com.data.controller;

import com.data.entity.Employee;
import com.data.model.EmployeeModel;
import com.googlecode.objectify.ObjectifyService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class ListEmployeeController extends HttpServlet {

    static {
        ObjectifyService.register(Employee.class);
    }

    EmployeeModel model = new EmployeeModel();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Employee> employee = model.listEmployee();
        req.setAttribute("employees", employee);
        req.getRequestDispatcher("/employee/list.jsp").forward(req, resp);
    }
}
