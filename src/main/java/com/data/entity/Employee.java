package com.data.entity;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

import java.util.Calendar;
import java.util.HashMap;

@Entity
public class Employee {
    @Id
    private long Id;
    @Index
    private String FullName;
    private String Birthday;
    private String Address;
    private String Position;
    private String Department;

    public Employee() {
        this.Id = Calendar.getInstance().getTimeInMillis();
    }

    public Employee(String fullName, String birthday, String address, String position, String department) {
        this.Id = Calendar.getInstance().getTimeInMillis();
        FullName = fullName;
        Birthday = birthday;
        Address = address;
        Position = position;
        Department = department;
    }

    public long getId() {
        return Id;
    }

    public void setId(long id) {
        Id = id;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getBirthday() {
        return Birthday;
    }

    public void setBirthday(String birthday) {
        Birthday = birthday;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getPosition() {
        return Position;
    }

    public void setPosition(String position) {
        Position = position;
    }

    public String getDepartment() {
        return Department;
    }

    public void setDepartment(String department) {
        Department = department;
    }

    // validate data
    public HashMap<String, String> validate() {
        HashMap<String, String> errors = new HashMap<>();
        if (FullName == null || FullName.isEmpty()) {
            errors.put("fullName", "Full Name is required!");
        }
        if (Birthday == null || Birthday.isEmpty()) {
            errors.put("birthDay", "Birth Day is required!");
        }
        if (Address == null || Address.isEmpty()) {
            errors.put("address", "Address is required!");
        }
        if (Position == null || Position.isEmpty()) {
            errors.put("position", "Position is required!");
        }
        if (Department == null || Department.isEmpty()) {
            errors.put("department", "Department is required!");
        }
        return errors;
    }

}
