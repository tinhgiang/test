package com.data.model;

import com.data.entity.Employee;
import com.googlecode.objectify.ObjectifyService;

import java.util.List;

import static com.googlecode.objectify.ObjectifyService.ofy;

public class EmployeeModel {

    static {
        ObjectifyService.register(Employee.class);
    }

    public boolean addEmployee(Employee employee) {
        ofy().save().entity(employee).now();
        return true;
    }

    public List<Employee> listEmployee() {
        List<Employee> listEmployee = ofy().load().type(Employee.class).list();
        return listEmployee;
    }
}
